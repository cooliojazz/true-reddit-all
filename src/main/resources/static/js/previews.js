function toggleImagePreview(el, id) {
    var preview = $("#" + id);
    if ($(el).html() == "+") {
        preview.attr("src", $(el).data("url"));
        $(el).html("-");
        preview.show();
        preview.on("mousedown", e => {
            $("body").on("mouseup." + id, e => ($("body").off("mouseup." + id), $("body").off("mousemove." + id)));
            var dragPos = {x: e.offsetX / parseInt(preview.width()), y: e.offsetY / parseInt(preview.height())};
            $("body").on("mousemove." + id, e => {
                var newSize = {x: Math.max((e.clientX - preview[0].getBoundingClientRect().x) / dragPos.x, 25), y: Math.max((e.clientY - preview[0].getBoundingClientRect().y) / dragPos.y, 25)};
                preview.css("max-width", newSize.x + "px");
                preview.css("max-height", newSize.y + "px");
            });
        });
        preview.on("dragstart", e => false);
    } else {
        $(el).html("+");
        preview.hide();
    }
}

function toggleVideoPreview(el, id) {
    if ($(el).html() == "+»") {
        if ($(el).data("url").endsWith(".gifv")) $(el).data("url", $(el).data("url").replace(".gifv", ".mp4"));
        $("#" + id).attr("src", $(el).data("reddit-video-url") || $(el).data("url"));
        $(el).html("-»");
        $("#" + id).show();
    } else {
        $(el).html("+»");
        $("#" + id).hide();
    }
}

function toggleEmbed(el, id) {
    if ($(el).html() == "+➔") {
        if ($(el).data("url").indexOf("redgifs.com/watch") > 0) $(el).data("url", $(el).data("url").replace("redgifs.com/watch", "redgifs.com/ifr"));
        if ($(el).data("url").indexOf("gfycat.com/") > 0) $(el).data("url", $(el).data("url").replace("gfycat.com", "gfycat.com/ifr"));
        if ($(el).data("url").indexOf("reddit.com/") > 0) $(el).data("url", $(el).data("comment-url").replace("reddit.com", "embed.reddit.com") + "?embed=true&theme=dark");

        $("#" + id).attr("src", $(el).data("url").replace("www.", ""));
        $(el).html("-➔");
        $("#" + id).show();
    } else {
        $(el).html("+➔");
        $("#" + id).hide();
    }
}

function toggleSelfPost(el, id) {
    if ($(el).html() == "+æ") {
        $.ajax("post/" + $(el).data("id"), {success: r => {
                // See: https://www.reddit.com/r/raerth/comments/cw70q/reddit_comment_formatting/
                let text = r.text.split(/\n\n|\r\n\r\n/);
                text = text.map(l => "<p>" + l + "</p>");
                let inQuote = false;
                for (let i = 0; i < text.length; i++) {
                    let line = text[i];
                    if (line.startsWith("<p>&gt;")) {
                        if (!inQuote) {
                            line = "<blockquote><p>" + line.substring(7);
                            inQuote = true;
                        } else {
                            line = "<p>" + line.substring(7);
                        }
                    } else {
                        if (inQuote) {
                            line += "</blockquote>";
                            inQuote = false;
                        }
                    }
                    text[i] = line
                            .replaceAll(/    \n/g, "<br>")
                            .replaceAll(/\[(.+)\]\((.+)\)/g, "<a href=\"$2\">$1</a>")
                            .replaceAll(/(?:\*\*|__)(.+)(?:\*\*|__)/g, "<b>$1</b>")
                            .replaceAll(/(?:\*|_)(.+)(?:\*|_)/g, "<i>$1</i>")
                            .replaceAll(/~~(.+)~~/g, "<del>$1</del>")
                            .replaceAll(/`(.+)`/g, "<code>$1</code>");
                }
                $("#" + id).html(text.join("") + (inQuote ? "</blockquote>" : ""));
                $(el).html("-æ");
                $("#" + id).show();
            }});
    } else {
        $(el).html("+æ");
        $("#" + id).hide();
    }
}