var url = new URL(window.location.href);
var subFilter = url.searchParams.get("subFilter") || "";

function updateFilter(filter) {
//	let url = new URL(window.location.href);
	let params = url.searchParams;
	if (filter == "none") {
		params.delete("filter");
	} else {
		params.set("filter", filter);
	}
//	window.location.href = url.href;
}

function updateNSFW(nsfw) {
//	let url = new URL(window.location.href);
	let params = url.searchParams;
	if (nsfw == 0) {
		params.delete("nsfw");
	} else {
		params.set("nsfw", nsfw == 1 ? true : false);
	}
//	window.location.href = url.href;
}

function updateSubFilter(input) {
	let subreddit = input.value;
	input.value = "";
	
	addSubFilter(subreddit);
}

function addSubFilter(subreddit) {
	if (subFilter.match("(^|,)" + subreddit + "(,|$)")) return;
	
	let el = $("#subListItem").contents().clone();
	el.find("#itemContent").html(subreddit);
	$("#subFilterList").append(el);

	subFilter += (subFilter.length > 0 ? "," : "") + subreddit;
	
	let params = url.searchParams;
	params.set("subFilter", subFilter);
}

function removeFilter(el) {
	let subreddit = el.nextElementSibling.innerHTML;
	el.parentNode.remove();
	
	let index = subFilter.indexOf(subreddit);
	subFilter = subFilter.slice(0, index - (index > 0 ? 1 : 0)) + subFilter.slice(index + subreddit.length + (index > 0 ? 0 : 1));

	let params = url.searchParams;
	if (subFilter.length == 0) {
		params.delete("subFilter");
	} else {
		params.set("subFilter", subFilter);
	}
}

function applyFilters() {
	window.location.href = url.href;
}

var shown = false;
function toggleFilters(el) {
	shown = !shown;
	if (shown) {
		el.innerHTML = "∨";
		$("#filters").css("display", "block");
	} else {
		el.innerHTML = "∧";
		$("#filters").css("display", "none");
	}
} 