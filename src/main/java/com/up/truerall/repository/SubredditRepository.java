package com.up.truerall.repository;

import com.up.truerall.model.Subreddit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ricky
 */
@Repository
public interface SubredditRepository extends PagingAndSortingRepository<Subreddit, Long> {
    
	@Query(value = "SELECT * FROM subreddit s WHERE NOT s.locked ORDER BY SQRT(s.subscribers) * POW(ABS(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(s.updated_posts_at)), 1.5) DESC LIMIT 1", nativeQuery = true)
	public Subreddit findFirstByPriority();
	
	@Query(value = "UPDATE subreddit as s SET locked = true FROM (SELECT id FROM subreddit WHERE NOT locked ORDER BY SQRT(subscribers) * POW(ABS(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(updated_posts_at)), 1.5) DESC LIMIT 1 FOR UPDATE) as row WHERE s.id = row.id RETURNING *", nativeQuery = true)
	public Subreddit findAndLockFirstByPriority();
	
}
