package com.up.truerall.repository;

import com.up.truerall.model.Post;
import java.util.Date;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ricky
 */
@Repository
public interface PostRepository extends PagingAndSortingRepository<Post, Long>, JpaSpecificationExecutor<Post> {
	
//    @Query("delete from User where firstName = :firstName")
    @Transactional(dontRollbackOn = {Exception.class})
//    @Transactional(Transactional.TxType.NEVER)
//    public int deleteFirst10000ByCreatedLessThan(Date created);
	@Modifying
    @Query(value = "DELETE FROM post WHERE id IN (SELECT id FROM post WHERE created < :created LIMIT :limit)", nativeQuery = true)
    public int deleteByCreatedLessThan(Date created, int limit);
    
    public static class PostSearchSpecifications {
        
//        public static Specification<Post> olderThan(Date date) {
//            return (root, query, builder) -> builder.lessThan(root.get("created"), date);
//        }
        
        public static Specification<Post> postHint(String postHint) {
            return postHint == null ? PostSearchSpecifications::safeNull : (root, query, builder) -> builder.equal(root.get("postHint"), postHint);
        }
        
        public static Specification<Post> thumbnail(String thumbnail) {
            return thumbnail == null ? PostSearchSpecifications::safeNull : (root, query, builder) -> builder.equal(root.get("thumbnail"), thumbnail);
        }
        
        public static Specification<Post> nsfw(Boolean nsfw) {
            return nsfw == null ? PostSearchSpecifications::safeNull : (root, query, builder) -> builder.equal(root.get("nsfw"), nsfw);
        }
        
        public static Specification<Post> excludeSubreddits(String[] subreddits) {
            return subreddits == null ? PostSearchSpecifications::safeNull :
                    (root, query, builder) -> builder.not(root.getJoins().stream().filter(j -> j.getAttribute().getName().equals("subreddit")).findAny().orElseGet(() -> root.join("subreddit")).get("name").in((Object[])subreddits));
        }
        
        private static Predicate safeNull(Root root, CriteriaQuery query, CriteriaBuilder builder) {
            return builder.conjunction();
        }
        
    }
}
