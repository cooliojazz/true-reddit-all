package com.up.truerall.config;

import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.ClientCredentialsOAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.RemoveAuthorizedClientOAuth2AuthorizationFailureHandler;
import org.springframework.security.oauth2.client.endpoint.DefaultClientCredentialsTokenResponseClient;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

/**
 *
 * @author Ricky
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().anonymous();
//        http.oauth2Login()
//            .tokenEndpoint().accessTokenResponseClient(accessTokenResponseClient()).and()
//            .userInfoEndpoint().userService(new OAuth2UserService<OAuth2UserRequest, OAuth2User>() {
//                @Override
//                public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
//                    return new DefaultOAuth2User(Collections.singletonList(() -> "read"), Collections.singletonMap("display_name", "test_user"), "display_name");
//                }
//            });
//        http.authorizeRequests().anyRequest().authenticated();
    }
	
    @Bean
    public WebClient defaultWebClient() {
		return WebClient.builder().defaultHeader("User-Agent", "server:true-reddit-all:v0.1.0").build();
    }
    
    @Bean("redditAppWebClient")
    public WebClient redditAppWebClient(ClientRegistrationRepository registrationRepo) {
		AuthorizedClientServiceOAuth2AuthorizedClientManager clientManager = new AuthorizedClientServiceOAuth2AuthorizedClientManager(registrationRepo, new InMemoryOAuth2AuthorizedClientService(registrationRepo));
		ClientCredentialsOAuth2AuthorizedClientProvider clientProvider = new ClientCredentialsOAuth2AuthorizedClientProvider();
		DefaultClientCredentialsTokenResponseClient tokenClient = new DefaultClientCredentialsTokenResponseClient();
		tokenClient.setRequestEntityConverter(new UserAgentOAuth2ClientCredentialsGrantRequestEntityConverter("server:true-reddit-all:v0.1.0"));
		clientProvider.setAccessTokenResponseClient(tokenClient);
		clientManager.setAuthorizedClientProvider(clientProvider);
		ServletOAuth2AuthorizedClientExchangeFilterFunction oauthFilter = new ServletOAuth2AuthorizedClientExchangeFilterFunction(clientManager);
		oauthFilter.setDefaultClientRegistrationId("redditServer");
		oauthFilter.setAuthorizationFailureHandler(new RemoveAuthorizedClientOAuth2AuthorizationFailureHandler((String clientRegistrationId, Authentication principal, Map<String, Object> attributes) -> {
				System.out.println("Login error?");
			}));
		return WebClient.builder()
				.apply(oauthFilter.oauth2Configuration())
				.exchangeStrategies(ExchangeStrategies.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(128 * 1024 * 1024)).build())
				.defaultHeader("User-Agent", "server:true-reddit-all:v0.1.0")
				.build();
    }
}
