package com.up.truerall.controller;

import com.up.truerall.model.Post;
import com.up.truerall.model.Subreddit;
import com.up.truerall.repository.PostRepository;
import com.up.truerall.repository.SubredditRepository;
import com.up.truerall.service.SubredditService;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Ricky
 */
@Controller
public class RedditController {
	
    // Just for reference for now; this is the raw of the built sort below
	//private static final Sort defaultSort = JpaSort.unsafe(Sort.Direction.DESC, "score * (1 + 10000 / GREATEST(p.subreddit.subscribers, 10)) * POW(2, (UNIX_TIMESTAMP(created) - UNIX_TIMESTAMP()) / 7200.0)");
    
    @Autowired
    public EntityManager entityManager;
    
    @Autowired
    public SubredditService subredditService;
    
    @Autowired
    public SubredditRepository subredditRepo;
    
    @Autowired
    public PostRepository postRepo;
    
    @GetMapping("/")
    public String index(@RequestParam(required = false) String filter, @RequestParam(required = false) Boolean nsfw, @RequestParam(required = false) String subFilter, Authentication auth, Model model, Pageable page) {
        model.addAttribute("page", page.getPageNumber());
        model.addAttribute("size", page.getPageSize() > 0 ? page.getPageSize() : 100);
        model.addAttribute("filter", filter);
        model.addAttribute("nsfw", nsfw);
        model.addAttribute("subFilter", subFilter);
        
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Post> query = cb.createQuery(Post.class);
        Root<Post> root = query.from(Post.class);
        Join<Post, Subreddit> join = root.join("subreddit");
        query.select(root);
        Expression sort = cb.prod(root.get("score"), cb.prod(cb.sum(1, cb.quot(10000, cb.function("GREATEST", Integer.class, join.get("subscribers"), cb.literal(10)))), cb.function("POW", Integer.class, cb.literal(2), cb.quot(cb.diff(cb.function("UNIX_TIMESTAMP", Integer.class, root.get("created")), cb.function("UNIX_TIMESTAMP", Integer.class)), cb.literal(7200.0)))));
        query.orderBy(cb.desc(sort));
        query.where(Specification
                    .where(PostRepository.PostSearchSpecifications.postHint(filter).or(PostRepository.PostSearchSpecifications.thumbnail(filter)))
                    .and(PostRepository.PostSearchSpecifications.nsfw(nsfw))
                    .and(PostRepository.PostSearchSpecifications.excludeSubreddits(subFilter == null ? null : subFilter.split(",")))
                    .toPredicate(root, query, cb));
        model.addAttribute("posts", entityManager.createQuery(query).setFirstResult(page.getPageNumber() * page.getPageSize()).setMaxResults(page.getPageSize()).getResultList());
		
        model.addAttribute("subreddits", subredditRepo.findAll());
        
        return "index";
    }
    
    @ResponseBody
    @GetMapping("/post/{id}")
    public Post index(@PathVariable Long id, Model model, Pageable page) {
        return postRepo.findById(id).orElse(null);
    }
    
    @GetMapping("/subs")
    public String subs(Authentication auth, Model model) {
        model.addAttribute("subreddits", subredditRepo.findAll(Sort.by("id").ascending()));
        return "subs";
    }
    
    @GetMapping("/dbinfo")
    public String dbinfo(Model model) {
        Object[] counts = (Object[])entityManager.createNativeQuery("SELECT (SELECT COUNT(*) FROM subreddit), (SELECT COUNT(*) FROM post)").getSingleResult();
        model.addAttribute("subreddits", counts[0].toString());
        model.addAttribute("posts", counts[1].toString());
        return "dbinfo";
    }
    
    @ResponseBody
    @GetMapping("/subs/add/list")
    public List<Subreddit> addSubs(@RequestParam String list) throws IOException {
        return subredditService.addSubreddits(Arrays.asList(list.split(",")));
    }
    
    @ResponseBody
    @GetMapping("/subs/add/search")
    public List<Subreddit> addSearchSubs(@RequestParam String search) throws IOException {
        return subredditService.addSearchSubreddits(search);
    }
}
