package com.up.truerall.util;

import java.util.Calendar;

/**
 *
 * @author Ricky
 */
public class FormattingUtil {
    
    public static String suffixTime(long ms) {
        long s = ms / 1000;
        if (s == 0) return ms + "ms";
        long m = s / 60;
        if (m == 0) return s + "s " + (ms % 1000) + "ms";
        long h = m / 60;
        if (h == 0) return m + "m " + (s % 60) + "s";
        long d = h / 24;
        if (d == 0) return h + "h " + (m % 60) + "m";
        long w = d / 7;
        if (w == 0) return d + "d " + (h % 60) + "h";
        return w + "w";
    }
    
    public static String timestamp() {
		return String.format("[%02d/%02d/%04d %02d:%02d:%02d] ",
				Calendar.getInstance().get(Calendar.MONTH) + 1,  Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), Calendar.getInstance().get(Calendar.SECOND));
    }
    
}
