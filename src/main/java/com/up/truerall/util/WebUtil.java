package com.up.truerall.util;

import org.springframework.stereotype.Component;

/**
 *
 * @author Ricky
 */
@Component
public class WebUtil {
	
	public static String createQueryString(int page, int size, String filter, String nsfw, String subFilter) {
		return "/?page=" + page + "&size=" + size + (filter != null ? "&filter=" + filter : "") + (nsfw != null ? "&nsfw=" + nsfw : "") + (subFilter != null ? "&subFilter=" + subFilter : "");
	}
    
}
