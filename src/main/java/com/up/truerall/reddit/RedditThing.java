package com.up.truerall.reddit;

/**
 *
 * @author Ricky
 */
public class RedditThing extends RedditObject {
	
	private String id;
	private String name;

	public String getId() {
		return id;
	}

    public long getLongId() {
        return Long.parseLong(id, 36);
    }

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
