package com.up.truerall.reddit;

import java.util.List;

/**
 *
 * @author Ricky
 */
public class RedditListing extends RedditObject {
	
	private List<RedditWrapper<RedditThing>> children;
	private String before;
	private String after;

	public List<RedditWrapper<RedditThing>> getChildren() {
		return children;
	}

	public void setChildren(List<RedditWrapper<RedditThing>> children) {
		this.children = children;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}
	
}
