package com.up.truerall.reddit;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 *
 * @author Ricky
 */
public class RedditWrapper<T extends RedditObject> {
    
	private Kind kind;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "kind")
    @JsonSubTypes({@JsonSubTypes.Type(value = RedditListing.class, name = "Listing"), @JsonSubTypes.Type(value = RedditThing.class, name = "more"), @JsonSubTypes.Type(value = RedditThing.class, name = "t1"),
        @JsonSubTypes.Type(value = RedditThing.class, name = "t2"), @JsonSubTypes.Type(value = RedditThing.class, name = "t3"), @JsonSubTypes.Type(value = RedditThing.class, name = "t4"),
        @JsonSubTypes.Type(value = RedditThing.class, name = "t5"), @JsonSubTypes.Type(value = RedditThing.class, name = "t6")})
    @JsonProperty("data")
	private T object;

	public Kind getKind() {
		return kind;
	}
    
	public void setKind(Kind kind) {
		this.kind = kind;
	}

	public T getObject() {
		return object;
	}
	public void setObject(T data) {
		this.object = data;
	}
	
	
	public static enum Kind {
		LISTING("Listing", RedditListing.class), MORE("more", RedditThing.class), T1("t1", RedditThing.class), T2("t2", RedditThing.class), T3("t3", RedditThing.class), T4("t4", RedditThing.class), T5("t5", RedditThing.class), T6("t6", RedditThing.class);
		
		private final String value;		
		private final Class<? extends RedditObject> model;

		private Kind(String value, Class<? extends RedditObject> model) {
			this.value = value;
			this.model = model;
		}

		public Class<? extends RedditObject> getModelClass() {
			return model;
		}

		@JsonValue
		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return value;
		}
		
	}
}
