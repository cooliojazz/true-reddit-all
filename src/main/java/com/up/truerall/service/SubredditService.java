package com.up.truerall.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.up.truerall.reddit.RedditListing;
import com.up.truerall.reddit.RedditThing;
import com.up.truerall.model.Subreddit;
import com.up.truerall.reddit.RedditWrapper;
import com.up.truerall.repository.SubredditRepository;
import com.up.truerall.util.FormattingUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Keeps the list of subreddits up to date
 * @author Ricky
 */
@Service
public class SubredditService {
    
    @Autowired
    public SubredditRepository subredditRepo;
    
    public ObjectMapper mapper;

    @Autowired
    @Qualifier("redditAppWebClient")
	private WebClient client;
	
	public SubredditService() {
		mapper = new ObjectMapper();
	}
    
    ////////////// Scheduled Jobs ///////////////////
	
    @Scheduled(cron = "0 0 0 ? * *")
//    @Scheduled(cron = "* * * ? * *")
    public void updateSubreddits() throws JsonProcessingException, IOException {
		long startTime = System.currentTimeMillis();
		List<Subreddit> subreddits = StreamSupport.stream(subredditRepo.findAll(Sort.by("id")).spliterator(), false).collect(Collectors.toList());
		for (int i = 0; i < subreddits.size(); i++) {
			Subreddit sub = subreddits.get(i);
			String res = client.get().uri("https://api.reddit.com/r/" + sub.getName() + "/about").retrieve().bodyToMono(String.class).onErrorResume(e -> Mono.just("")).block();
			if (res.isBlank()) {
				continue;
			}
			RedditThing updatedSub = mapper.readValue(res, new TypeReference<RedditWrapper<RedditThing>>() {}).getObject();
			Subreddit s = new Subreddit();
			s.setId(updatedSub.getLongId());
			s.setName(updatedSub.getData().get("display_name").toString());
			s.setSubscribers((Integer)updatedSub.getData().get("subscribers"));
			s.setUpdatedPostsAt(sub.getUpdatedPostsAt());
			subredditRepo.save(s);
			System.out.println(FormattingUtil.timestamp() + "Updated " + sub.getName());
		}
		System.out.println(FormattingUtil.timestamp() + "Updated all subreddits in " + (System.currentTimeMillis() - startTime) + "ms");
    }
    
    ///////////////////////////////////
	
    public List<Subreddit> addSubreddits(List<String> subreddits) throws JsonProcessingException, IOException {
		long startTime = System.currentTimeMillis();
		List<Subreddit> currentSubreddits = StreamSupport.stream(subredditRepo.findAll(Sort.by("id")).spliterator(), false).collect(Collectors.toList());
        List<Subreddit> newSubreddits = new ArrayList<>();
		for (String subreddit : subreddits) {
            if (!currentSubreddits.stream().anyMatch(s -> s.getName().equals(subreddit))) {
                String res = client.get().uri("https://api.reddit.com/r/" + subreddit + "/about").retrieve().bodyToMono(String.class).onErrorResume(e -> Mono.just("")).block();
                if (res.isBlank()) {
                    continue;
                }
                RedditThing updatedSub = mapper.readValue(res, new TypeReference<RedditWrapper<RedditThing>>() {}).getObject();
                Subreddit sub = new Subreddit();
                sub.setId(updatedSub.getLongId());
                sub.setName(updatedSub.getData().get("display_name").toString());
                sub.setSubscribers((Integer)updatedSub.getData().get("subscribers"));
                sub.setUpdatedPostsAt(new Date(0));
                subredditRepo.save(sub);
                newSubreddits.add(sub);
                System.out.println(FormattingUtil.timestamp() + "Added " + sub.getName());
            }
		}
		System.out.println(FormattingUtil.timestamp() + "Added " + newSubreddits.size() + " new subreddits in " + (System.currentTimeMillis() - startTime) + "ms");
        return newSubreddits;
    }
    
    public List<Subreddit> addSearchSubreddits(String search) throws JsonProcessingException, IOException {
		long startTime = System.currentTimeMillis();
		RedditListing listing = null;
        List<Subreddit> newSubreddits = new ArrayList<>();
		do {
//			String res = client.get().uri("https://api.reddit.com/subreddits" + (listing != null ? "?after=" + listing.getAfter() : "")).retrieve().bodyToMono(String.class).block();
			//This VV seems to only return like 5 pages, and ^^ seems to not give nsfw subreddits. Why is this hard :(
			String res = client.get().uri("https://www.reddit.com/subreddits/search.json?q=" + search + "&nsfw=1&include_over_18=on" + (listing != null ? "&after=" + listing.getAfter() : "")).retrieve().bodyToMono(String.class).block();
			listing = mapper.readValue(res, new TypeReference<RedditWrapper<RedditListing>>() {}).getObject();
			for (RedditWrapper<RedditThing> sub : listing.getChildren()) {
				if (!subredditRepo.existsById(sub.getObject().getLongId())) {
                    Subreddit s = new Subreddit();
                    s.setId(sub.getObject().getLongId());
                    s.setName(sub.getObject().getData().get("display_name").toString());
                    s.setSubscribers(sub.getObject().getData().get("subscribers") != null ? (Integer)sub.getObject().getData().get("subscribers") : -1);
                    s.setUpdatedPostsAt(new Date(0));
					subredditRepo.save(s);
					newSubreddits.add(s);
                    System.out.println(FormattingUtil.timestamp() + "Added " + s.getName());
				}
			}
			System.out.println(FormattingUtil.timestamp() + "Checked items up to " + listing.getAfter());
		} while (listing.getAfter() != null && !listing.getAfter().isEmpty());
		System.out.println(FormattingUtil.timestamp() + "Added " + newSubreddits.size() + " new subreddits in " + (System.currentTimeMillis() - startTime) + "ms");
        return newSubreddits;
    }
    
}
