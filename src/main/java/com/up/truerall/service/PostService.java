package com.up.truerall.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.up.truerall.model.Post;
import com.up.truerall.reddit.RedditListing;
import com.up.truerall.reddit.RedditThing;
import com.up.truerall.model.Subreddit;
import com.up.truerall.reddit.RedditWrapper;
import com.up.truerall.repository.PostRepository;
import com.up.truerall.repository.SubredditRepository;
import com.up.truerall.util.FormattingUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.core.publisher.Mono;

/**
 * Keeps the list of posts up to date
 * @author Ricky
 */
@Service
public class PostService {
    
    @Autowired
    public SubredditRepository subredditRepo;
    
    @Autowired
    public PostRepository postRepo;
    
    public ObjectMapper mapper;

    @Autowired
    @Qualifier("defaultWebClient")
	private WebClient client;

    @Autowired
    @Qualifier("redditAppWebClient")
	private WebClient authedClient;
	
	public PostService() {
		mapper = new ObjectMapper();
	}
    
    ////////////// Scheduled Jobs ///////////////////
	
//    @Scheduled(cron = "0 0/10 * ? * *")
//    @Scheduled(cron = "0/10 * * ? * *")
    @Scheduled(fixedRate = 10000)
    public void getPosts() throws JsonProcessingException, IOException {
        System.out.println(FormattingUtil.timestamp() + "Updating a subreddit...");
		Subreddit sub = subredditRepo.findAndLockFirstByPriority();
		if (sub != null) {
//			sub.setLocked(true);
//			sub = subredditRepo.save(sub);
			try {
				updateSubredditPosts(sub);
			} catch (Exception e) {
				sub.setLocked(false);
				subredditRepo.save(sub);
				throw e;
			}
//			sub.setLocked(false);
//			subredditRepo.save(sub);
		} else {
			System.out.println(FormattingUtil.timestamp() + "Failed to get a subreddit to update?");
		}
    }
    
//    public void getPostsOld() throws JsonProcessingException, IOException {
//		long startTime = System.currentTimeMillis();
//		List<Subreddit> subreddits = StreamSupport.stream(subredditRepo.findAll(/*Sort.by("id").descending()*/).spliterator(), false).collect(Collectors.toList());
//		for (int i = 0; i < subreddits.size(); i++) {
//			Subreddit sub = subreddits.get(i);
//            updateSubredditPosts(sub);
//			double completion = ((double)i / subreddits.size());
//			System.out.println("Updated posts for r/" + sub.getName() + " (" + String.format("%.2f", completion * 100) + "% complete, estimated " + FormattingUtil.suffixTime((long)((System.currentTimeMillis() - startTime) / 1000.0 * (1 / completion - 1))) + " remaining)");
//		}
//		System.out.println("Updated all posts in " + (System.currentTimeMillis() - startTime) / 1000.0 + "s");
//    }
	
    private static final long POST_REMOVAL_DAYS = 10;
    
//    @Scheduled(cron = "0 0 0 ? * *")
//    @Scheduled(cron = "* * * ? * *")
//    @Scheduled(fixedRate = 300000)
    @Scheduled(fixedRate = 30000)
    public void cleanOldPosts() throws JsonProcessingException, IOException {
		try {
			long startTime = System.currentTimeMillis();
	//		postRepo.deleteAll(postRepo.findAll(Specification.where(PostRepository.PostSearchSpecifications.olderThan(new Date(System.currentTimeMillis() - POST_REMOVAL_DAYS * 24 * 60 * 60 * 1000))), PageRequest.of(0, 10000)));
			int count = postRepo.deleteByCreatedLessThan(new Date(System.currentTimeMillis() - POST_REMOVAL_DAYS * 24 * 60 * 60 * 1000), 10000);
			System.out.println(FormattingUtil.timestamp() + "Cleared " + count + " old posts in " + FormattingUtil.suffixTime(System.currentTimeMillis() - startTime));
		} catch (Exception e) {
			System.err.println(FormattingUtil.timestamp() + "Error clearing old posts: " + e.getLocalizedMessage());
		}
    }
    
    ///////////////////////////////////
    
    private void updateSubredditPosts(Subreddit sub) throws JsonProcessingException {
		long startTime = System.currentTimeMillis();
        System.out.println(FormattingUtil.timestamp() + "Updating posts for r/" + sub.getName() + "...");
        String res = client.get().uri("https://www.reddit.com/r/" + sub.getName() + "/.json").retrieve().bodyToMono(String.class).onErrorResume(e -> Mono.just("")).block();
        if (res.isBlank()) {
            // Maybe do something else later, but for now at least this keeps it running
            sub.setUpdatedPostsAt(new Date());
			sub.setLocked(false);
            subredditRepo.save(sub);
            System.out.println(FormattingUtil.timestamp() + "Failed to update r/" + sub.getName() + ".");
            return;
        }
        RedditListing listing = mapper.readValue(res, new TypeReference<RedditWrapper<RedditListing>>() {}).getObject();
        ArrayList<Post> posts = new ArrayList<>();
        for (RedditWrapper<RedditThing> post : listing.getChildren()) {
            Post p = new Post();
            p.setId(post.getObject().getLongId());
            p.setSubreddit(sub);
            p.setPermalink((String)post.getObject().getData().get("permalink"));
            p.setCreated(new Date(((Double)post.getObject().getData().get("created")).longValue() * 1000));
            p.setText((String)post.getObject().getData().get("selftext"));
            p.setUrl((String)post.getObject().getData().get("url"));
            p.setPostHint((String)post.getObject().getData().get("post_hint"));
            p.setTitle((String)post.getObject().getData().get("title"));
            p.setThumbnail((String)post.getObject().getData().get("thumbnail"));
            p.setUps((Integer)post.getObject().getData().get("ups"));
            p.setDowns((Integer)post.getObject().getData().get("downs"));
            p.setScore((Integer)post.getObject().getData().get("score"));
            p.setComments((Integer)post.getObject().getData().get("num_comments"));
            p.setAuthor((String)post.getObject().getData().get("author"));
            p.setVideo((Boolean)post.getObject().getData().get("is_video"));
            p.setNsfw((Boolean)post.getObject().getData().get("over_18"));
            if (post.getObject().getData().get("media") != null)
                p.setRedditVideoUrl(
                        ((Map<String, Map<String, String>>)post.getObject().getData().get("media"))
                        .getOrDefault("reddit_video", Collections.singletonMap("fallback_url", ""))
                        .get("fallback_url")
                    );
            posts.add(p);
        }
        postRepo.saveAll(posts);
        sub.setUpdatedPostsAt(new Date());
		sub.setLocked(false);
        subredditRepo.save(sub);
        System.out.println(FormattingUtil.timestamp() + "Updated posts for r/" + sub.getName() + " in " + FormattingUtil.suffixTime(System.currentTimeMillis() - startTime) + ".");
    }
}
