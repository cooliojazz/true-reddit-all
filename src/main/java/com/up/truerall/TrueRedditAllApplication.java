package com.up.truerall;

import com.up.truerall.service.SubredditService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TrueRedditAllApplication {
	
	@Value("${import-subreddits}")
	private boolean importSubreddits;
	
    @Autowired
    private SubredditService service;
    
	public static void main(String[] args) {
		SpringApplication.run(TrueRedditAllApplication.class, args);
	}
    
    @PostConstruct
    public void start() throws Exception {
        if (importSubreddits) service.addSubreddits(new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/subreddits.csv"))).lines().collect(Collectors.toList()));
    }

}