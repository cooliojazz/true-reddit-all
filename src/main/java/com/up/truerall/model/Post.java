package com.up.truerall.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Ricky
 */
@Entity
public class Post {

    @Id
    private Long id;
	private Date created;
	@Column(length = 1024)
	private String permalink;
	@Column(length = 2048)
	private String url;
	@Column(columnDefinition = "TEXT")
	private String text;
	private String postHint;
	@Column(length = 400)
	private String title;
	@Column(columnDefinition = "TEXT")
	private String thumbnail;
	private int score;
	private int ups;
	private int downs;
	private int comments;
	private String author;
	private boolean video;
	private String redditVideoUrl;
	private boolean nsfw;
	
    @JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Subreddit subreddit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPostHint() {
		return postHint;
	}

	public void setPostHint(String postHint) {
		this.postHint = postHint;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getScore() {
		return score;
	}

//    private static final String[] abvs = {"", "k", "M", "G", "T", "P", "E", "Z", "Y", "?"};
//    
//    public String getAbbreviatedScore() {
//        int exp = Math.max(Math.min((int)(Math.log(score) / Math.log(1000)), abvs.length - 1), 0);
//        return String.format("%.2f", score / Math.pow(1000, exp)) + abvs[exp];
//    }
    public String getAbbreviatedScore() {
        return score < 10000 ? score + "" : String.format("%.2f", score / 1000.0) + "k";
    }

	public void setScore(int score) {
		this.score = score;
	}

	public int getUps() {
		return ups;
	}

	public void setUps(int ups) {
		this.ups = ups;
	}

	public int getDowns() {
		return downs;
	}

	public void setDowns(int downs) {
		this.downs = downs;
	}

	public int getComments() {
		return comments;
	}

	public void setComments(int comments) {
		this.comments = comments;
	}

	public Subreddit getSubreddit() {
		return subreddit;
	}

	public void setSubreddit(Subreddit subreddit) {
		this.subreddit = subreddit;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setVideo(boolean video) {
		this.video = video;
	}

	public boolean isVideo() {
		return video;
	}

	public String getRedditVideoUrl() {
		return redditVideoUrl;
	}

	public void setRedditVideoUrl(String redditVideoUrl) {
		this.redditVideoUrl = redditVideoUrl;
	}
    
	public boolean isNsfw() {
		return nsfw;
	}

	public void setNsfw(boolean nsfw) {
		this.nsfw = nsfw;
	}
    
	public boolean isImage() {
		return "image".equals(postHint) || url.endsWith(".jpg") || url.endsWith(".jpeg") || url.endsWith(".png") || url.endsWith(".gif");
	}
	
	public boolean saferIsVideo() {
		return video || url.endsWith(".gifv") || url.endsWith(".webm") || url.endsWith(".mp4");
	}
    
	public boolean isEmbed() {
		return url.contains("redgifs.com/watch") || url.contains("reddit.com/gallery") || url.contains("gfycat.com/");
	}
    
	public boolean isSelf() {
		return ("self".equals(getPostHint()) || "self".equals(getThumbnail())) && getText().length() > 0;
	}
}
