package com.up.truerall.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import jdk.jfr.Timestamp;
import org.hibernate.annotations.ColumnDefault;

/**
 *
 * @author Ricky
 */
@Entity
public class Subreddit {

    @Id
    private Long id;
    private String name;
    private int subscribers;
    @Timestamp
    private Date updatedPostsAt;
	@ColumnDefault("false")
	private boolean locked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(int subscribers) {
		this.subscribers = subscribers;
	}

    public Date getUpdatedPostsAt() {
        return updatedPostsAt;
    }

    public void setUpdatedPostsAt(Date updatedPostsAt) {
        this.updatedPostsAt = updatedPostsAt;
    }

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}
    
}
