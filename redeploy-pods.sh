#!/bin/bash

kubectl.exe patch deployment -n reddit-all reddit-all --patch "spec:
  replicas: 0" --insecure-skip-tls-verify
kubectl.exe patch deployment -n reddit-all reddit-all --patch "spec:
  replicas: 2" --insecure-skip-tls-verify
