FROM openjdk:14-jdk-alpine
ARG JAR_FILE=target/*.jar
WORKDIR /opt/reddit-all
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Xmx750M","-jar","app.jar"]